/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import datos.operaciones;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author pima_
 */
public class calculatorTest {
    
   operaciones calculator1 = new operaciones();
   //Test suma
@Test
public void testSuma1(){
assertEquals(5, calculator1.suma(2,3));
}
@Test
public void testSuma2(){
assertEquals(6, calculator1.suma(3,3));
}
//Test resta
@Test
public void testResta1(){
assertEquals(2, calculator1.resta(5,3));
}
@Test
public void testResta2(){
assertEquals(3, calculator1.resta(6,3));
}
//Test multiplicacion
@Test
public void testMultipicacion1(){
assertEquals(3, calculator1.multiplicacion(1,3));
}
@Test
public void testMultiplicacion2(){
assertEquals(27, calculator1.multiplicacion(9,3));
}

//Test division

@Test
public void testdivision1(){
assertEquals(2, calculator1.division(6,3));
}
@Test
public void testdivision2(){
assertEquals(3, calculator1.division(9,3));
}

//Test exponencia
@Test
public void testexponenciacion1(){
assertEquals(36, calculator1.exponenciacion(6,2));
}
@Test
public void testexponenciacion2(){
assertEquals(4, calculator1.exponenciacion(2,2));
}
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
